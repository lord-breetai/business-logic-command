/*
 *
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.jatun.open.tools.blcmd.interceptor;

import com.jatun.open.tools.blcmd.exception.HandledBusinessLogicException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * @author Ivan Alban
 */
@Aspect
@Component
class BLCmdExecuteMethodInterceptor implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(BLCmdExecuteMethodInterceptor.class);

    private ApplicationContext applicationContext;

    @Autowired
    private ExecutorServiceLoader serviceLoader;

    @Around("within(com.jatun.open.tools.blcmd.core.BusinessLogicCommand+) && execution(public void execute())")
    public void executeMethodAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object commandInstance = joinPoint.getTarget();

        Class<? extends BusinessLogicCommandExecutorService> executorServiceClazz = serviceLoader.load(commandInstance);

        BusinessLogicCommandExecutorService service = applicationContext.getBean(executorServiceClazz);

        MetadataLoader metadata = new MetadataLoader() {
            @Override
            protected Object getCommandInstance() {
                return commandInstance;
            }
        };

        metadata.process();

        Long start = (new Date()).getTime();
        try {
            service.execute(new Work() {
                @Override
                protected void proceed() throws Throwable {
                    joinPoint.proceed();
                }

                @Override
                protected void preProceed() throws Throwable {
                    if (null != metadata.getPreExecuteMethod()) {
                        Method preExecute = metadata.getPreExecuteMethod();

                        ReflectionUtils.makeAccessible(preExecute);
                        ReflectionUtils.invokeMethod(preExecute, commandInstance);
                    }
                }

                @Override
                protected void postProceed() throws Throwable {
                    if (null != metadata.getPostExecuteMethod()) {
                        Method postExecute = metadata.getPostExecuteMethod();

                        ReflectionUtils.makeAccessible(postExecute);
                        ReflectionUtils.invokeMethod(postExecute, commandInstance);
                    }
                }
            });
        } catch (Throwable throwable) {
            if (throwable instanceof HandledBusinessLogicException) {
                throw ((HandledBusinessLogicException) throwable).getSource();
            }

            throw throwable;
        } finally {
            Long end = (new Date()).getTime();

            LOGGER.debug("The execution for {} is: [{} ms]", commandInstance.getClass(), (end - start));
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
