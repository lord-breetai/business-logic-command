package com.jatun.open.tools.test.blcmd;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Ivan Alban
 */
@Configuration
@ComponentScan("com.jatun.open.tools.test.blcmd")
@Import({com.jatun.open.tools.blcmd.Config.class})
public class Config {
}
