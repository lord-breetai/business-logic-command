package com.jatun.open.tools.test.blcmd.lombok;

import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import com.jatun.open.tools.test.blcmd.AbstractTest;
import com.jatun.open.tools.test.blcmd.commons.PrototypeDependency;
import com.jatun.open.tools.test.blcmd.commons.SingletonDependency;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

/**
 * @author Ivan Alban
 */
public class LombokGetterTest extends AbstractTest {

    @Autowired
    private SingletonDependency singletonDependency;

    @Autowired
    private PrototypeDependency prototypeDependency;

    @Autowired
    private LombokGetterCmd command;

    @Autowired
    private BusinessLogicCommandFactory factory;

    @Test
    public void autowiredAnnotatedTest() {
        command.execute();

        Assert.assertEquals(singletonDependency, command.getSingletonDependency());
        Assert.assertNotEquals(prototypeDependency, command.getPrototypeDependency());
        Assert.assertNotNull(command.getEntity());
        Assert.assertFalse(StringUtils.isEmpty(command.getValue()));
    }

    @Test
    public void commandFactoryTest() {
        LombokGetterCmd instance = factory.createInstance(LombokGetterCmd.class);
        instance.execute();

        Assert.assertEquals(singletonDependency, instance.getSingletonDependency());
        Assert.assertNotEquals(prototypeDependency, instance.getPrototypeDependency());
        Assert.assertNotNull(instance.getEntity());
        Assert.assertFalse(StringUtils.isEmpty(instance.getValue()));
    }
}
